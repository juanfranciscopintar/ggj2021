extends Popup

signal pause_button_pressed

func _process(_delta):
	if Input.is_action_just_pressed("pause"):
			emit_signal("pause_button_pressed")

func _on_Pause_pause_button_pressed():
	if !get_tree().paused == true:
		get_tree().paused = true
		show()
	else:
		hide()
		get_tree().paused = false
