extends Obstacle

class_name MetalDisk

func _on_MetalDisk_area_entered(area):
	if area is Bullet:
		area.queue_free()
		.change_hp(1)
