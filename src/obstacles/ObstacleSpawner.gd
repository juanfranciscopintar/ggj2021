extends Node2D

var Asteroid = preload("res://obstacles/Asteroid.tscn")
var BrokenExhaust = preload("res://obstacles/BrokenExhaust.tscn")
var MetalDisk = preload("res://obstacles/MetalDisk.tscn")
var SolarPanel = preload("res://obstacles/SolarPanel.tscn")
var obstacles = [Asteroid, BrokenExhaust, MetalDisk, SolarPanel]
export (int) var spawn_delay_limit = 4

func _ready():
	randomize()

func spawn_obstacle():
	# Position offset from (0, 0) to point of spawn
	var pos_offset = Vector2(randf() * (get_owner().screen_size.x - 128) + 64, 0) # TODO: Remove hard coded values
	# Index for array containing different types of obstacles
	var obstacle_index = randi() % obstacles.size()
	# Boolean for determining if mirrored animated sprites or not
	var is_mirrored = true if randi() % 2 else false
	
	var obstacle = obstacles[obstacle_index].instance()
	obstacle.set_global_position(pos_offset)
	add_child(obstacle)
	obstacle.get_node("AnimatedSprite").flip_h = is_mirrored

func _on_SpawnTimer_timeout():
	spawn_obstacle()
	$SpawnTimer.wait_time = (randi() % spawn_delay_limit) + 1 
