extends Area2D

class_name Obstacle

var Bullet = load("res://character/Bullet.gd")

var speed = 100
var hp = 3

func _process(_delta):
	if global_position.y > 1024: # TODO: remove hard coded number
		queue_free()

func _physics_process(delta):
	global_position.y += speed * delta

func change_hp(amount):
	hp -= amount
	if hp <= 0:
		speed = 0
		$CollisionShape2D.queue_free()
		$AnimatedSprite.animation = "explosion"
		#$AudioStreamPlayer.play()
		yield($AnimatedSprite, "animation_finished")
		queue_free()
