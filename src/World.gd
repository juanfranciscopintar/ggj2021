extends Node

signal distance_changed(new_distance)
signal reached_end_of_game()

var Bullet = preload("res://character/Bullet.tscn")
var Ship = preload("res://character/Ship.tscn")

var ambient_noise = preload("res://assets/sound_fx/drone.wav")
var music = preload("res://assets/theme_songs/ingame_theme.ogg")
var win_music = preload("res://assets/theme_songs/win_theme.ogg")
var loss_music = preload("res://assets/theme_songs/loss_theme.ogg")
var explosion_fx = preload("res://assets/sound_fx/explosion.wav")

var player_hit_sounds = [
	preload("res://assets/sound_fx/hit_01.wav"), 
	preload("res://assets/sound_fx/hit_02.wav"),
	preload("res://assets/sound_fx/hit_03.wav")]

onready var screen_size = get_viewport().size
export (int) var distance_to_ship

func _ready():
	$AmbientNoise.stream = ambient_noise
	$Music.stream = music
	start_new_game()

func _physics_process(delta):
	distance_to_ship -= delta
	emit_signal("distance_changed", distance_to_ship)

func clear_all_obstacles():
	var obstacles = $ObstacleSpawner.get_children()
	for i in obstacles:
		if i is Obstacle:
			i.queue_free()

func start_new_game():
	# TODO: Spawn player under screen, move upwards
	$AmbientNoise.play()
	$Music.play()
	$HUD.visible = true
	$Player.is_locked = false
	distance_to_ship = 60
	# TODO: Delay spawner
	$ObstacleSpawner/SpawnTimer.start()

func game_over():
	$HUD.visible = false
	$Background.material.set_shader_param("scroll_speed", 0)
	$BackgroundAlpha.material.set_shader_param("scroll_speed", 0)
	$AmbientNoise.stop()
	$Music.stream = loss_music
	$Music.play()
	$GameOverPopup.visible = true

func game_win():
	# Stop music
	$Music.stop()
	# Hide HUD
	$HUD.visible = false
	# Lock player input
	emit_signal("reached_end_of_game") # Player node then locks input
	$ObstacleSpawner/SpawnTimer.paused = true
	clear_all_obstacles()
	# Spawn/animate ship towards screen
	var ship = Ship.instance()
	add_child(ship)
	$Ship/AnimationPlayer.play("arrival")
	# Play victory music
	$Music.stream = win_music
	$Music.play()
	yield($Ship/AnimationPlayer, "animation_finished")
	# Wait for player to move to door
	$Ship/AnimatedSprite.play("closing_door")
	# Animate player towards ship
	$WinPopup.visible = true
	# Play credits

func _on_Player_spawn_bullet(location):
	var bullet = Bullet.instance()
	bullet.global_position = location
	add_child(bullet)

func _on_Player_died():
	game_over()

func _on_Player_hit():
	$SoundFX.stream = player_hit_sounds[randi() % 3]
	$SoundFX.play()

func _on_Timer_timeout():
	game_win()
