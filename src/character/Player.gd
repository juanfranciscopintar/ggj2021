extends Area2D

class_name Player

onready var gun = $Gun
onready var sprites = $AnimatedSprite

signal spawn_bullet(location)
signal hit()
signal health_changed()
signal died()

export (int) var speed = 200
export (int) var max_hp = 100
export (int) var hp

var velocity = Vector2.ZERO # Initialize velocity vector with (0, 0)
var is_locked
var move_player_to_ship = false

func _ready():
	hp = max_hp
	is_locked = false

func _physics_process(delta):

	# TODO: Add ease-up for breaking inertia, then normalize
	
	if !is_locked:
		var input_x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
		var input_y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
		
		if sprites.animation != "hit":
			if input_x > 0:
				sprites.animation = "right"
			elif input_x < 0:
				sprites.animation = "left"
			else:
				sprites.animation = "idle"
				
		velocity.x += input_x
		velocity.y += input_y
		
		if Input.is_action_just_pressed("shoot"):
			shoot_gun()
	
	if move_player_to_ship:
		position = lerp(position, Vector2(300, 128), delta)
		if position.y < 200:
			modulate.a = lerp(modulate.a, 0.001, delta * speed)
	
	#if position == Vector2(300, 128) and move_player_to_ship:
	#	modulate.a = lerp(modulate.a, 0, delta * speed * 5)
	
	position += velocity * speed * delta
	
	position.x = clamp(position.x, 32, 568) # TODO: Get screen size to avoid hard coding values
	position.y = clamp(position.y, 0, 960) #
	
	if position.x == 32 or position.x == 568: #
		velocity.x = 0
	if position.y == 0 or position.y == 960: #
		velocity.y = 0

func change_hp(amount):
	hp -= amount
	emit_signal("health_changed", hp)
	if hp <= 0:
		queue_free()
		emit_signal("died")

func shoot_gun():
	emit_signal("spawn_bullet", gun.global_position)

func _on_Player_area_entered(area):
	if area.is_in_group("obstacles"):
		emit_signal("hit")
		sprites.play("hit")
		change_hp(10)
		yield(sprites, "animation_finished")
		sprites.animation = "idle"

func _on_World_reached_end_of_game():
	is_locked = true
	velocity = Vector2.ZERO
	sprites.animation = "idle"
	# Wait for ship animation to finish
	yield(get_tree().create_timer(5), "timeout")
	move_player_to_ship = true
