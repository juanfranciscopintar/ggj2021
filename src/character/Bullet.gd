extends Area2D

class_name Bullet

var speed = 800

func _process(_delta):
	if global_position.y < 0:
		queue_free()

func _physics_process(delta):
	global_position.y += -speed * delta 
